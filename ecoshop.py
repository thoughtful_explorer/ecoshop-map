#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import simplekml
import geocoder
import re

#Set filename/path for KML file output
kmlfile = "ecoshop.kml"
#Set KML schema name
kmlschemaname = "ecoshop"
#Set page URL
pageURL = "https://www.ecoshop.be/nl/onze-winkels"
#Start a session with the given page URL
session = requests.Session()
page = session.get(pageURL)
#Soupify the HTML
soup = BeautifulSoup(page.content, 'html.parser')
#Specify the store div class to get all the store information
stores = soup(class_="result")

#Initialize kml object
kml = simplekml.Kml()
#Add schema, which is required for a custom address field
schema = kml.newschema(name=kmlschemaname)
schema.newsimplefield(name="address",type="string")

#Iterate through datamap for each store
for store in stores:
    #Get the name of the store
    storename = store.a.h2.get_text()
    #Get the address of the store, which is the second p tag in the div
    storeaddress = store.findAll('p')[1].get_text()
    #Nominatim is no help for now (see https://github.com/osm-search/Nominatim/issues/679), so we must modify some of the addresses for geocoding to be successful
    if re.match("P. ",storeaddress): #P. Corneliskaai 26, 9300 Aalst, België
        storeaddress = "Pierre Corneliskaai  26, 9300 Aalst, België"
    elif re.match("Blauwe Toren",storeaddress): #Blauwe Toren, Monnikenwerve 11, 8000 Brugge, België
        storeaddress = "Monnikenwerve 11, 8000 Brugge, België"
    elif re.match("Sint ",storeaddress): #Sint Sebastiaanstraat 70, 8800 Roeselare, België
        storeaddress = "Sint-Sebastiaansstraat 70, 8800 Roeselare, Belgien"
    #Run the geocode
    geo = geocoder.osm(storeaddress)
    #Get coordinates from the geocode
    lat = geo.y
    lng = geo.x
    #First, create the point name and description in the kml
    point = kml.newpoint(name=storename,description="thrift store")
    #Then, add the custom "SimpleData" address field to kml file with proper hierarchical kml data structure
    point.extendeddata.schemadata.schemaurl = kmlschemaname
    simpledata = point.extendeddata.schemadata.newsimpledata("address",storeaddress)
    #Finally, add coordinates to the feature
    point.coords=[(lng,lat)]
#Save the final KML file
kml.save(kmlfile)
